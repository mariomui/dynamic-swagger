import os
import time
from subprocess import PIPE, Popen


def main():
    p = Popen(["poetry", "run", "start:server"], stdout=PIPE)
    # p is 3 in the beginning
    print(p.stdout.fileno(), "what am i")
    os.set_blocking(p.stdout.fileno(), False)
    start = time.time()
    while True:
        # first iteration always produces empty byte string in non-blocking
        # mode
        for i in range(2):
            line = p.stdout.readline()
            if line != b"":
                print(f"{line.decode('utf8')}")
            time.sleep(0.5)
        if time.time() > start + 500:
            break
    p.terminate()
