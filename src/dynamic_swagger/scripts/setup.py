import os
import time
from multiprocessing import Process
from subprocess import PIPE, Popen

from package.my_utils import run_command

# ❯ poetry env use python
# poetry shell


def shell():
    p = run_command(
        "poetry shell",
    )
    print(p.poll())


def start_server():
    split_cmd = ""
    p = Popen(split_cmd, stdout=PIPE)
    os.set_blocking(p.stdout.fileno(), False)
    start = time.time()
    while True:
        # first iteration always produces empty byte string in non-blocking \
        # mode
        for i in range(2):

            line = p.stdout.readline()
            if line != b"":
                print(f"{line.decode('utf8')}")
            time.sleep(0.5)
        if time.time() > start + 500:
            break
    p.terminate()


def start():
    process = Process(target=shell)
    process.start()
    # shell(start_server)
    process.join()

    # completed = call(f'poetry run start:server', shell=True)
    process_s = Process(target=start_server)
    process_s.start()
    process_s.join()


# poetry env info --path
