from subprocess import PIPE, CalledProcessError, Popen, TimeoutExpired
from sys import stdout as sys_stdout
from typing import List


def run_command(cmd: str, has_output=False, returns_shell=False):
    """runs a bash command"""
    p: Popen

    split_cmd: List[str] = cmd.split(" ")
    # dont call shell
    if has_output and returns_shell is False:
        p = Popen(split_cmd, stdout=PIPE, stderr=PIPE, text=True)
        (stdout, stderr) = p.communicate()
        if stderr is None:
            return stdout
        return None
    else:
        try:
            p = Popen(
                split_cmd,
                stdout=sys_stdout,
                cwd=".",
                stderr=PIPE,
            )
            if p.poll():
                return p

        except CalledProcessError as exc:
            print(
                f"Command {split_cmd} failed because the process "
                f"did not return a successful return code.\n{exc}"
            )
        except TimeoutExpired as exc:
            print(f"Command {split_cmd} timed out.\n {exc}")
        except Exception as ex:
            print(f"ex::{ex}")
        finally:
            return p


# i should really run tests
