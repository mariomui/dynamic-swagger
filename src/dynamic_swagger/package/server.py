from connexion import FlaskApp

# from flask import Flask
options = {"swagger_ui": True}

# src/dynamic_swagger/open_api/index.yaml
app: FlaskApp = FlaskApp(__name__, port=5000, specification_dir="../open_api/")


app.add_api("index.yaml", base_path="/api", options=options)
# @app.route('/')
# def hello_world():
#     return 'Hello, World!'


def run():
    app.run(host="0.0.0.0")


# app.run(host=0.0.0.0)
if __name__ == "__main__":
    run()
