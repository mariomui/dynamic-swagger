
# Purpose

* The purpose of this repos is to create a base python server for experimentation
* Do not code multiple python poetry environments at the same time.
  * you could, except my notifications setup doesn't code for that use case

## Requirements

### CORE

* MAC
* brew
  * install list:
    * pyenv
    * $ \text{pyenv-virtualenv} $
    * `pipx`
      * you'll need `pipx` to install poetry
      * I did it both ways. pipx is more hassle free. Relative imports and all that.

### Shell/Status Notifications

* zsh
* ohmyzsh
  * <https://ohmyz.sh/>
  * framework for managing zsh configurations
* p10k
  * <https://github.com/romkatv/powerlevel10k>
  * theme for zsh

## IDE Requirements

* Pycharm
  * set the detect local environment setting in python
    * make sure you are on the correct env.
  * also `poetry run pip freeze > requirements.txt` so the imports run properly
* VSCode
  * python-environment-manager extension
  * python `intellisense` extension
  * "python.terminal.activateEnvInCurrentTerminal": false,
    * i like to turn this to false as i need to make sure that I'm in the right environment manually.
      * `which python`
        * inside the shell the python should be directed to the `pypoetry` directory
  * pylance extension
  * set the interpreter to home into the correct env.
  
## Steps To Use (FIRST TIME)

* pyenv install 3.8
  * makes sure you have 3.8
* pipx install poetry
* poetry env use python
  * auto generates a virtual env for you based on 3.8
  * links the python interpreter path to the poetry-env
  * `poetry env info --path` to double check
* poetry shell
  * creates a shell
  * sources activate
  * Checking
    * `echo $POETRY_ACTIVE`
      * should be 1
    * `poetry env list`
      * should show a env that has an env with Activated appointed to the end.

* poetry install
  * install all dependencies
  
## Installing Dependencies

* poetry add [etc]
* poetry run pip freeze > requirements.txt
  * see section labeled IDE for the why-for.

## Caveats

* Don't use brew, use pipx
* I need to call poetry shell from python
  * I cannot. Poetry shell being called does not call source the activate command to activate the poetry-managed shell
* relative imports in python are weird in python 3
  * without poetry you would hack setup tools, with poetry you define the module root
    * <https://python-poetry.org/docs/pyproject/#packages>

It should look like this:
```sh
❯ poetry shell
Spawning shell within $HOMELibrary/Caches/pypoetry/virtualenvs/dynamic-swagger-FUMVy5eA-py3.8
❯ . $HOME/Library/Caches/pypoetry/virtualenvs/dynamic-swagger-FUMVy5eA-py3.8/bin/activate
```
it takes like 3 seconds

## Deploying

* use `pipx`
* instructions will come later.

## Niceties

### Terminal Status Bars

* if you have p10K, you can run a function that will always check your current directory if you are running a poetry project. It will display a warning if you left the poetry shell running.
  * place `code` in your`~/.p10k.zsh` file
    * code is found below under the bullet point labeled`p10k code`
  * put `my_poetry` under `POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS`
  * Visually:
    * Within project root: you will see a green prompt with your poetry shell env name
    * Under or above project root: It will warn you that it is still running
    * If shell is inactive, it will tell you.
  * WARNING:
    * you might want to open a ton of shells
    * you may be tempted to use pyenv
    * DON'T.
      * sub dependencies are hard to keep track of.
      * manually creating a env via pyenv is insane.
      * Deployment is a pain.
        * just learn `pipx`/isolated dependencies now and save yourself the trouble.
        * Python Aphorism#13
          * There should be one-- and preferably only one --obvious way to do it.

  * `p10k code`
    ```sh
    function prompt_my_poetry() {
        local POETRY_ENV=
        if [ ! -f './pyproject.toml' ]
        then
          POETRY_ENV=
        else
          POETRY_ENV="$(poetry env list | grep -e Activated | sed 's/ .*//' | head -1)"
        fi

        if [ ! -z ${POETRY_ENV} ] && [ -z ${POETRY_ACTIVE} ]
        then
          p10k segment -b 1 -f 3 -i '⭐' -t "poetry inactive"
        elif [ ! -z ${POETRY_ENV} ] && [ ! -z $POETRY_ACTIVE ] 
        then
          p10k segment -b 2 -f 3 -i '⭐' -t "$POETRY_ENV active"
        elif [ -z ${POETRY_ENV} ] && [ ! -z $POETRY_ACTIVE ]
        then
          # if your shell is active and you are outside of the folder.
          p10k segment -b 3 -f 0 -i '⭐' -t "POETRY is still running"
        fi

        if 
        then
        else
        fi

        #bg is background, setting that to 0 to 255 
        #fg is foreground
        # i is icon
        # t is text
        # https://robotmoon.com/256-colors/ 
      }
    ```

## Future

* Testing
  * [ ] <https://github.com/wntrblm/nox>
  * [ ] How do people lock down peer dependencies when they are using old pymodules?
    * say I'm using Flask 2020version, and Connexion 2020, the peer dependencies are all *borked*.
    * <https://github.com/python-poetry/poetry/issues/697>
      * there are best practices in this thread that needs to be codified.
      * also, python calls these sub dependencies not peer dependencies.
* Lint-ing
  * black
  * use `darken` for formatting selected blocks
    * <https://github.com/psf/black/issues/830>
  * auto-pep
  * flake8
* include all my settings so a brand new person can have all the auto formatting and good dx experience.
* augment the p10k my_poetry prompt

## LINT-ING

* I've decided on flake8 and pylint
  * pylint because its the oldest and has the most support
  * flake8 because it has a lot of edge cases.
  * black and darker with diff to control the DX autoformatting experience.
  * There's a lot of pathing (manual) that is needed to make this all work.
    * [ ] need to automate the vscode environment
* Worker settings
* Lets use darker so that it diffs the main branch allowing selected blocks to be block formatted.
```json
{
  "editor.defaultFormatter": null,
  "python.formatting.provider": "black",
  "python.formatting.blackPath": "[INSERT DARKER path]",
  "editor.formatOnSave": true,
  "python.linting.flake8Enabled": true,
  "[python]": {
    "editor.formatOnSave": true,
  },
  "python.terminal.activateEnvInCurrentTerminal": false,
  "[python]": {
    "editor.defaultFormatter": "ms-python.black-formatter"
  },
  // "python.poetryPath": "poetry"
}
```
