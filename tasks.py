from invoke import task


@task
def deploy(c):
    c.run("poetry shell")
    c.run("sleep 5")
    c.run("poetry invoke serve")


@task
def install(c):
    c.run("poetry shell && poetry install")


@task
def serve(c):
    c.run("poetry run gunicorn -- --bind 0.0.0.0:5000 wsgi:app")


@task
def version(c):
    c.run("poetry version patch")

    c.run("git tag $(poetry version --short)")
    c.run("git add -A")
    c.run("git commit -m \'Update version\'")

    c.run("git push --tags")
    c.run("git push origin main")
